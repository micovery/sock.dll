[TOC]

# sock.dll#

  * This is a C/C++ extension for the [Arma 3 RV4 Engine](http://www.arma3.com/features/engine)
  * The extension allows synchronous communication between the SQF code, and a remote server over a TCP socket.
  * The extension uses **SOCK-TCP** protocol to communicate with remote server
  * The extension uses **SOCK-SQF** protocol to communicate with SQF.

```                    
     +-------+                +----------+                +----------+
     |       |                |          |                |          |
     |  SQF  | <= SOCK-SQF => | SOCK.DLL | <= SOCK-TCP => |  REMOTE  |
     |       |                |          |                |          |
     +-------+                +----------+                +----------+         
 
```


## Changelog ##
  * 0.0.2 - Port extension to Linux
  * 0.0.1 - Initial release

## Prerequisites ##
  * On Windows 7 (64bit)
    * Download and install [Microsoft Visual C++ 2010 Redistributable Package (32-Bit x86)](http://www.microsoft.com/en-us/download/details.aspx?id=26999)  
    .
  * On Ubuntu 14.04 (64bit)
    * Install 32bit runtime libraries for libc6, and libstdc++6, and libgcc1
    *  ```
sudo apt-get install lib32stdc++6 libc6-i386 lib32gcc1
    ```


## Building the extension ##
  * On Windows 7 (64bit)
    * Clone the git repository, or [download](https://bitbucket.org/micovery/sock.dll/get/v0.0.2.zip) and extract the repository zip in your file system
    * Open up the Visual Studio project, and build from within the IDE  
    .
  * On Ubuntu 14.04 (64bit)
    *  ```
sudo apt-get update
    ```
    *  ```
sudo apt-get install git build-essential g++-multilib
    ```
    *  ```
git clone https://micovery@bitbucket.org/micovery/sock.dll.git
    ```
    *  ```
cd sock.dll
    ```
    *  ```
make
    ```


## Installation ##
  * On Windows 7 (64bit)
    * Build the sock.dll extension, or [download](https://bitbucket.org/micovery/sock.dll/raw/v0.0.2/Release/sock.dll) the binary 
    * Copy the sock.dll extension into the Arma 3 process working directory (i.e. C:\Program Files (x86)\Steam\steamapps\common\Arma 3)  
      .
  * On Ubuntu 14.04 (64bit)
    * Build the sock.so extension, or [download](https://bitbucket.org/micovery/sock.dll/raw/v0.0.2/Release/sock.so) the binary
    * Copy the sock.so extension into the Arma 3 process working directory (i.e. /home/steam/steamcmd/arma3/)  
      .

## How to use ##

  * When starting the Arma 3 game client/server, pass the following required command line arguments:  
    .  
    * *-sock_host*=**host**  
        This is the host name, or IPv4/IPv6 address of the **SOCK-TCP** server to connect to  
        .  

    * *-sock_port*=**port**  
        This is the port number of the **SOCK-TCP** server to connect to  
        .  

  * You can optionally pass the following command line arguments as well:  
    .  
    * *-sock_log*=**logfile**  
        This is the path to the file to use for logging.  
        If not provided, it defaults to sock_${PID}.log, where ${PID} is the windows process ID of the Arma 3 process.  
        .  
    * *-sock_debug*=**true**  
        If set to true, the extension prints additional information in the log file, that can be useful for debugging purposes.  
        If not provided, or set to false, only minimal information is printed to the extension log file.  
        (Do not use this on production code, the additional messages printed in the log significantly slow the response time)  
        .  

## Passing arguments using command-line ##

1. Open a command line window
2. Start the game directly, and pass the arguments
```
"C:\Program Files (x86)\Steam\steamapps\common\Arma 3\arma3.exe" -sock_host=::1 -sock_port=1337 
```
.  
Note, you can also pass the following additional arguments to the game, so that it loads faster, and display script errors (if you are running the extension on a dedicated server, these are not needed).
```
-nosplash -world=empty -showScriptErrors
```
.  

## Passing arguments using Steam client ##

You can also set the command line arguments from Steam client:  

1. Navigate to the **Games Library** tab inside the Steam Client  
1. Right click on Arma 3, and select **Properties** from the context menu  
1. Click on the **General** tab within the **Properties** window  
1. Click on the **Set Launch Options** button  
1. Append the desired command line arguments in the text-box within the **Launch Options** window  


## SOCK-SQF Protocol ##

This is the protocol that the sock.dll extension uses to communicate with the SQF side.

In Arma 3, the RV4 Game Engine has a buffer size of 10240 bytes to send responses to the SQF side.  
Because of this limitation, the sock.dll extension use a generic *multi-chunk* format for sending response data the SQF side.  
.  

  * On success, the response is split into multiple chunks, and sent to SQF side as an array of chunk addresses  
  .  
  ```
 ["0x19308ff0","0x19313020","0x1931d050","0x19327080",...]
  ```  
  .  
  The SQF side must fetch each chunk, and concatenate them.  
  .  

  * On failure, (socket error), the error message is sent to the SQF side as a string  
  .  
  ```
  "Error String"
  ```  
  .  

* Once a chunk of data has been fetched, the memory is immediately freed.  
* Chunks that have not been fetched after 10 seconds, are automatically freed.  
* The extension will only send responses for chunk addresses it has issued itself (to prevent memory access violations)  
.  

The sample code below shows how to handle a *multi-chunk* response.  
.  

```
sock_get_response = {
  if (isNil "_this" || {typeName _this != typeName ""}) exitWith {};

  private["_request"];
  _request = _this;

  private["_extension"];
  _extension = "sock";

  private["_response_info"];
  _response_info = call compile (_extension callExtension _request);

  if (isNil "_response_info") exitWith {
    (format["protocol error: Was expecting response of typeName of %1, but got %2", (typeName []), "nil"]) call sock_log_severe;
  };

  private["_response_type"];
  _response_type = typeName _response_info;

  if (_response_type == typeName "") exitWith {
    ("error: " + _response_info) call sock_log_severe;
  };

  if (_response_type != typeName []) exitWith {
   (format["protocol error: Was expecting response of typeName of %1, but got %2", (typeName []), _response_type]) call sock_log_severe;
  };

  private["_chunks", "_chunk_count"];
  _chunks = _response_info;
  _chunk_count = count(_chunks);

  private["_i", "_response"];
  _i = 0;
  _response = "";

  //retrieve all the response chunks, and concatenate them
  while {_i < _chunk_count } do {
      private["_address", "_data"];
     _address = _chunks select _i;
     _data = _extension callExtension (_address);
     _response = _response + _data;
     _i = _i + 1;
  };

  if (isNil "_response") exitWith {};
  _response
};

sock_log_severe = {
  if (isNil "_this" || {typeName _this != typeName ""}) exitWith {};
  diag_log ("sock: severe: " + _this);
};

```  
. 


## SOCK-TCP Protocol ##

This is the protocol that the sock.dll extension uses to communicate with the remote side.  

When communicating with the remote side, the sock.dll extension acts as the client.



```

//First send the request message
client: send: request-header-foo (4 bytes, unsigned long, network byte order, length of the body that follows)
client: send: request-body-foo (variable bytes, actual request message data)

//Then, wait for server to send response message
client: recv: response-header-foo (4 bytes, unsigned long, network byte order, length of the body that follows)
client: recv: response-body-foo (variable bytes, actual response message data)

```

* Note that the protocol allows client to send a zero-length message (only header).  
* Upon receipt of a zero-length message, the server must ignore it, and continue processing additional request messages.  
* This is useful for the client to test whether the socket is still active, without incurring in side-effects on the server-side.
.